APPENDIX A - CITABLE INFRACTIONS
================================

*With reference to Applicable Rules, Complaint Requirements & Potential Consequences for Specific Violations Shaded areas are new citable infractions*


1  Unauthorized Access to MLS
-----------------------------

+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
|        |                                               |                                   | Applicable             | Participant    | Staff      |
|SECTION |   Violation Description                       |   Fine/Action                     | Rules                  | Subscriber     | Authorized |
|        |                                               |                                   | Section(s)             | Complaint      | to issue   |
|        |                                               |                                   |                        | Required       | Citation   |
+========+===============================================+===================================+========================+================+============+
| 1.1    | Use of MLS System by Unauthorized Party       | Refer to Hearing Panel            | 4, 5, 12.1, 12.2, 12.3 | No             | Yes        |
|        |                                               |                                   |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 1.1.1  | Filing of False Participation Waiver          | $500 fine per incident            | 5.1.6                  | No             | Yes        |
|        |                                               | (in addition to 6                 |                        |                |            |
|        |                                               | months MLS Fees for               |                        |                |            |
|        |                                               | each sales person in              |                        |                |            |
|        |                                               | violation of Waiver               |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 1.1.2  | Failure of Participant to Notify the MLS      | 1st Violation - Notice            |                        |                |            |
|        | within 10 days of Termination, Transfer, or   | to Comply                         |                        |                |            |
|        | Addition of an Associate Under Participant's  | Failure to comply - $250          |                        |                |            |
|        | License.                                      |                                   | 4.4                    | Yes            | Yes        |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 1.1.3  | Failure to meet all of the requirements as a  | Loss of MLS Services              |                        |                |            |
|        | Participant or Subscriber.                    | (Note: Subscriber - loss          | 4.1, 4.2               | No             | Yes        |
|        |                                               | of MLS services,                  |                        |                |            |
|        |                                               | Participant - loss of             |                        |                |            |
|        |                                               | entire office MLS                 |                        |                |            |
|        |                                               | Services.                         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+

1.2  Misuse of MLS Information
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
|        |                                               |                                   | Applicable             | Participant    | Staff      |
|SECTION |   Violation Description                       |   Fine/Action                     | Rules                  | Subscriber     | Authorized |
|        |                                               |                                   | Section(s)             | Complaint      | to issue   |
|        |                                               |                                   |                        | Required       | Citation   |
+========+===============================================+===================================+========================+================+============+
| 1.2.1  | Reproducing, Distributing or Displaying MLS   | $500 fine/ Refer to               | 12.15.2                | No             | No         |
|        | information for Unauthorized Purposes         | Hearing Panel                     |                        |                |            | 
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 1.2.2  | Reproducing, Distributing or Displaying       | 1st Violation - Letter of Warning | 12.15.1, 12.19.15      | Yes            | Yes        |
|        | Unauthorized Portions of the MLS Database     | 2nd Violation - $100              |                        |                |            |
|        |                                               | 3rd Violation - $200              |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 1.2.3  | Unauthorized Computer Download or Transmission| Refer to Hearing Panel & Legal    | 12.15, 12.19.1         | No             | No         |
|        | of data                                       | Council                           |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 1.2.4  | Use of MLS in your website address or search  | Letter of Warning                 | 12.18                  | No             | Yes        |
|        | name                                          | 1st Violation $300                |                        |                |            |
|        |                                               | 2nd Violation $500                |                        |                |            |
|        |                                               | 3rd Violation loss of IDX & MLS   |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+


2  Loading Listings and Reporting Status Changes By Deadline
------------------------------------------------------------


+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
|        |                                               |                                   | Applicable             | Participant    | Staff      |
|Section |   Violation Description                       |   Fine/Action                     | Rules                  | Subscriber     | Authorized |
|        |                                               |                                   | Section(s)             | Complaint      | to issue   |
|        |                                               |                                   |                        | Required       | Citation   |
+========+===============================================+===================================+========================+================+============+
| 2.1    | Listing Not Loaded Within 3 days of Start     | 1st Violation - Letter of Warning | 7.5                    | Yes            | Yes        |
|        | of Listing                                    | 2nd Violation - $100 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $200 Fine         |                        |                |            | 
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 2.2    | Listing Waiver Not Submitted to MLS Within    | 1st Violation - Letter of Warning | 7.6                    | Yes            | Yes        |
|        | 3 days of Start Date of Listing               | 2nd Violation - $100 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $200 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+

2.3  Status Changes Not Reported By Deadline
--------------------------------------------

+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
|        |                                               |                                   | Applicable             | Participant    | Staff      |
|Section |   Violation Description                       |   Fine/Action                     | Rules                  | Subscriber     | Authorized |
|        |                                               |                                   | Section(s)             | Complaint      | to issue   |
|        |                                               |                                   |                        | Required       | Citation   |
+========+===============================================+===================================+========================+================+============+
| 2.3.1  | Sale Not Reported by within 1 day after Close | 1st Violation - Letter of Warning | 10.2                   | Yes            | Yes        |
|        | of Escrow                                     | 2nd Violation - $100 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $200 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 2.3.2  | Pending Sale Not Reported within 1 day after  | 1st Violation - Letter of Warning | 10.2                   | Yes            | Yes        |
|        | Ratification of Sales Contract                | 2nd Violation - $100 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $200 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 2.3.3  | Contingent Sale or Lease Not within 1 day     | 1st Violation - Letter of Warning | 10.2                   | Yes            | Yes        |
|        | after Ratification of Sales Contract          | 2nd Violation - $100              |                        |                |            |
|        |                                               | 3rd Violation - $200 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 2.3.4  | Cancellation of Pending Sale Not Reported     | 1st Violation - Letter of Warning | 10.3                   | Yes            | Yes        |
|        | with day after Written Cancellation           | 2nd Violation - $100 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $200 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 2.3.5  | Any Change in the original listing Agreement, | 1st Violation - Letter of Warning | 7.8                    | Yes            | Yes        |
|        | be in the MLS within one (1) day.             | 2nd Violation - $100 Fine         |                        |                |            |
|        | Examples:                                     | 3rd Violation - $200 Fine         |                        |                |            |
|        | * Price Change                                |                                   |                        |                |            |
|        | * Extending expiration date                   |                                   |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 2.3.6  | Resolution of Contingencies (Court            | 1st Violation - Letter of Warning | 10.2                   | Yes            | Yes        |
|        | confirmation, sale of buyer property) Not     | 2nd Violation - $100 Fine         |                        |                |            |
|        | Reported within 1 day after Resolution        | 3rd Violation - $200 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+

2.4  Unilateral Contractual Offer
---------------------------------

+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
|        |                                               |                                   | Applicable             | Participant    | Staff      |
|Section |   Violation Description                       |   Fine/Action                     | Rules                  | Subscriber     | Authorized |
|        |                                               |                                   | Section(s)             | Complaint      | to issue   |
|        |                                               |                                   |                        | Required       | Citation   |
+========+===============================================+===================================+========================+================+============+
| 2.4.1  | Unilateral Contractual Offer. Must make some  | 1st Violation - Letter of Warning | 7.12                   | No             | Yes        |
|        | offer of compensation                         | 2nd Violation - $200 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $300 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+

3  Reporting and Accuracy of Information
----------------------------------------
Submission of Listings That Do Not Satisfy the Requirements of the MLS Rules
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
|        |                                               |                                   | Applicable             | Participant    | Staff      |
|Section |   Violation Description                       |   Fine/Action                     | Rules                  | Subscriber     | Authorized |
|        |                                               |                                   | Section(s)             | Complaint      | to issue   |
|        |                                               |                                   |                        | Required       | Citation   |
+========+===============================================+===================================+========================+================+============+
| 3.1    | Must disclose potential short sales. Not      | 1st Violation - Letter of Warning | 7.28                   | No             | Yes        |
|        | allowed to place any reduction conditions on  | 2nd Violation - $200 Fine         |                        |                |            |
|        | on compensation offered through the MLS       |                                   |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 3.1.1  | Failure to Properly Classify Listing Type and | 1st Violation - Letter of Warning | 7.3                    | No             | Yes        |
|        | Category                                      | 2nd Violation - $200 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $300 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 3.1.2  | Entry of Inaccurate or Non-Text Information   | 1st Violation - Letter of Warning | 8.3                    | Yes            | Yes        |
|        | Anywhere in a Listing                         | 2nd Violation - $200 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $300 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 3.1.3  | Listing Information Incomplete or Not Kept    | 1st Violation - Letter of Warning | 7.11                   | No             | Yes        |
|        | Current                                       | 2nd Violation - $200 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $300 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 3.1.4  | Failure to Enter Accurate Information in a    | 1st Violation - Letter of Warning | 7.11, 8.3              | Yes            | Yes        |
|        | Required Field                                | 2nd Violation - $200 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $300 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 3.1.5  | Using a Data Field for a Purpose Other Than   | TBD                               | 8.3                    |                |            |
|        | its Intended Use                              |                                   |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 3.1.6  | Failure to Correct Incomplete or Inaccurate   | 1st Violation - $300 Fine         | 8.2, 8.3               | No             | Yes        |
|        | Information Within one (1) day after          | 2nd Violation - $400 Fine         |                        |                |            |
|        | Notification by Staff                         | 3rd Violation - $500 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 3.1.7  | Submission of Duplicate Listings by the Same  | 1st Violation - Letter of Warning | 7.3                    | No             | Yes        |
|        | Participant within the Same Key Property Type | 2nd Violation - $100 Fine         |                        |                |            |
|        | or Category                                   | 3rd Violation - $200 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 3.1.8  | Entry of Non-Property Specific Virtual Media  | 1st Violation - Letter of Warning | 12.15.2                | No             | Yes        |
|        | Link Anywhere in a Listing (with the exception| 2nd Violation - $100 Fine         |                        |                |            |
|        | of confidential remarks)                      | 3rd Violation - $200 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 3.1.9  | On Photos: Display of Agent Contact           | 1st Violation - Letter of Warning | 11.5                   | No             | Yes        |
|        | Information, such as Email, Addresses, Website| 2nd Violation - $250 Fine         |                        |                |            |
|        | Addresses, or other Non-Property Descriptive  | 3rd Violation - $350 Fine         |                        |                |            |
|        | Text                                          |                                   |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 3.1.10 | Use of Photographs on a Listing Without Proper| 1st Violation - Letter of Warning | 11.5                   | Yes            | Yes        |
|        | Authorization                                 | 2nd Violation - $250 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $350 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 3.1.11 | Failure to enter photo or submit photo/image  | 1st Violation - Letter of Warning | 11.5a                  | No             | Yes        |
|        | waiver to EBRDI within 3 days of entry of     | 2nd Violation - $50 Fine          |                        |                |            |
|        | listing into MLS                              | 3rd Violation - $200 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 3.2    | Failure to Withdraw a Cancelled listing by the| 1st Violation - Letter of Warning | 7.9                    | Yes            | Yes        |
|        | end of the next business day after            | 2nd Violation - $250 Fine         |                        |                |            |
|        | ratification of cancellation                  | 3rd Violation - $350 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 3.3    | Failure to Report the Correct Sales Price on  | 1st Violation - Letter of Warning | 8.1                    | Yes            | Yes        |
|        | a Closed Sale                                 | 2nd Violation - $100 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $200 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+

3.4  Purposely Manipulating the MLS System to Circumvent the Rules
------------------------------------------------------------------

+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
|        |                                               |                                   | Applicable             | Participant    | Staff      |
|Section |   Violation Description                       |   Fine/Action                     | Rules                  | Subscriber     | Authorized |
|        |                                               |                                   | Section(s)             | Complaint      | to issue   |
|        |                                               |                                   |                        | Required       | Citation   |
+========+===============================================+===================================+========================+================+============+
| 3.4.1  | Entry of Inaccurate or Prohibited Information | 1st Violation - Letter of Warning | 8.3                    | No             | Yes        |
|        |                                               | 2nd Violation - $100 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $200 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 3.4.2  | Posting of a Listing to the MLS Without       | $250 Fine for each occurrences    | 8.1                    | Yes            | Yes        |
|        | Having a Written Listing Agreement            |                                   |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 3.4.3  | Submitting a Listing as Withdrawn/Cancelled   | 1st Violation - Letter of Warning | 8.3                    | Yes            | Yes        |
|        | When Not Withdrawn/Cancelled by Seller        |                                   |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 3.4.4  | Extending a Listing Without Written           | 1st Violation - Letter of Warning | 8.3                    | Yes            | Yes        |
|        | Authorization from the Seller                 | 2nd Violation - $150 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $300 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 3.4.5  | Failure to Report a Dual or Variable Rate     | 1st Violation - Letter of Warning | 7.22                   | No             | Yes        |
|        | Commission                                    | 2nd Violation - $100 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $200 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 3.4.6  | Refusal to Report Accurate Information or to  | 1st Violation - Letter of Warning | 8.3                    | No             | Yes        |
|        | Correct Inaccurate Information                | 2nd Violation - $300 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $400 Fine         |                        |                |            |
|        |                                               | 4th Violation - $500 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 3.4.7  | Failure to Present Offers. Listing Broker     | 1st Violation - Letter of Warning | 9.4                    | Yes            | Yes        |
|        | must present all offers to Seller(s)          | 2nd Violation - $500 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $1000 Fine        |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 3.4.8  | Failure to Submit all Counter Offers.  Listing| 1st Violation - Letter of Warning | 9.5                    | Yes            | Yes        |
|        | Broker must present all offers to Seller(s)   | 2nd Violation - $500 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $1000 Fine        |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+

4  Remarks
----------
4.1 Misuse of Remarks:
~~~~~~~~~~~~~~~~~~~~~~

+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
|        |                                               |                                   | Applicable             | Participant    | Staff      |
|Section |   Violation Description                       |   Fine/Action                     | Rules                  | Subscriber     | Authorized |
|        |                                               |                                   | Section(s)             | Complaint      | to issue   |
|        |                                               |                                   |                        | Required       | Citation   |
+========+===============================================+===================================+========================+================+============+
| 4.1.1  | Publishing Presentation Offer Date and/or Time| 1st Violation - Letter of Warning | 12.5.2f                | Yes            | Yes        |
|        | Without Written Instructions From Seller(s)   | 2nd Violation - $100 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $200 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 4.1.2  | Failure to Include "For Comps Only" in First  | 1st Violation - Letter of Warning | 10.2                   | No             | Yes        |
|        | Line of Confidential Remarks of Listings      | 2nd Violation - $100 Fine         |                        |                |            |
|        | Entered For That Purpose                      | 3rd Violation - $200 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 4.1.3  | Failure to Update Offer Date within 1 day     | 1st Violation - Letter of Warning | 12.5.2f                | Yes            | Yes        |
|        | After Receiving Revised Written Instructions  | 2nd Violation - $100 Fine         |                        |                |            |
|        | From Seller(s)                                | 3rd Violation - $200 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+

4.2 Misuse of Public Remarks - Publishing any of the following in public remarks:
---------------------------------------------------------------------------------

+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
|        |                                               |                                   | Applicable             | Participant    | Staff      |
|Section |   Violation Description                       |   Fine/Action                     | Rules                  | Subscriber     | Authorized |
|        |                                               |                                   | Section(s)             | Complaint      | to issue   |
|        |                                               |                                   |                        | Required       | Citation   |
+========+===============================================+===================================+========================+================+============+
| 4.2.1  | Telephone Numbers                             | 1st Violation - Letter of Warning | 12.5.1                 | No             | Yes        |
|        |                                               | 2nd Violation - $250 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $350 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 4.2.2  | Names, Including Company Names                | 1st Violation - Letter of Warning | 12.5.1                 | No             | Yes        |
|        |                                               | 2nd Violation - $250 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $350 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 4.2.3  | Email Addresses                               | 1st Violation - Letter of Warning | 12.5.1                 | No             | Yes        |
|        |                                               | 2nd Violation - $250 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $350 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 4.2.4  | Websites                                      | 1st Violation - Letter of Warning | 12.5.1                 | No             | Yes        |
|        |                                               | 2nd Violation - $250 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $350 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 4.2.5  | Security Code changes                         | 1st Violation - $250 Fine         | 12.5.1                 | No             | Yes        |
|        |                                               | 2nd Violation - $350 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $450 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 4.2.6  | Lockbox Code changes                          | 1st Violation - $250 Fine         | 12.5.1                 | No             | Yes        |
|        |                                               | 2nd Violation - $350 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $450 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 4.2.7  | Virtual Tours                                 | 1st Violation - Letter of Warning | 12.5.1                 | No             | Yes        |
|        |                                               | 2nd Violation - $250 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $350 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 4.2.8  | Vacancy of Property                           | 1st Violation - Letter of Warning | 12.5.1                 | No             | Yes        |
|        |                                               | 2nd Violation - $250 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $350 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 4.2.9  | Title or Escrow Instructions                  | 1st Violation - Letter of Warning | 12.5.1                 | No             | Yes        |
|        |                                               | 2nd Violation - $250 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $350 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+

4.3 Misuse of Confidential Remarks
----------------------------------

+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
|        |                                               |                                   | Applicable             | Participant    | Staff      |
|Section |   Violation Description                       |   Fine/Action                     | Rules                  | Subscriber     | Authorized |
|        |                                               |                                   | Section(s)             | Complaint      | to issue   |
|        |                                               |                                   |                        | Required       | Citation   |
+========+===============================================+===================================+========================+================+============+
| 4.3.1  | Publishing Security Codes Without Seller's    | 1st Violation - Letter of Warning | 12.5.2                 | Yes            | Yes        |
|        | Written Permission                            | 2nd Violation - $250 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $350 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 4.3.2  | Caution: Title or escrow information may be   |                                   |                        |                |            |
|        | entered in confidential remarks; however,     |                                   |                        |                |            |
|        | participants/subscribers should note that any |                                   |                        |                |            |
|        | verbiage, which implies a requirement to use a|                                   |                        |                |            |
|        | specific title company or escrow service, may |                                   |                        |                |            |
|        | be a violation of RESPA. You are advised to   |                                   |                        |                |            |
|        | seek legal counsel for specific advice when   |                                   |                        |                |            |
|        | using such verbiage.                          |                                   |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 4.3.3  | Publishing Reference to Licensed              | 1st Violation - $100 Fine         | 12.5.2                 | No             | Yes        |
|        | Non-Subscribers Except in the Case of         | 2nd Violation - $200 Fine         |                        |                |            |
|        | Reciprocal Listings                           | 3rd Violation - $350 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+

5  Miscellaneous
----------------
5.1 Showings and Access
~~~~~~~~~~~~~~~~~~~~~~~

+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
|        |                                               |                                   | Applicable             | Participant    | Staff      |
|Section |   Violation Description                       |   Fine/Action                     | Rules                  | Subscriber     | Authorized |
|        |                                               |                                   | Section(s)             | Complaint      | to issue   |
|        |                                               |                                   |                        | Required       | Citation   |
+========+===============================================+===================================+========================+================+============+
| 5.1.1  | Showing Access. Properties entered into the   | 1st Violation - Letter of Warning | 9.1.1                  | Yes            | Yes        |
|        | system must be available to show within 3 days| 2nd Violation - $100 Fine         |                        |                |            |
|        | subject to any tenants rights.                | 3rd Violation - $200 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 5.1.2  | Use of Lockbox Key by Someone Other than      | Refer to appropriate Association  | 13.2                   | Yes            | No         |
|        | Registered Key holder.                        |                                   |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 5.1.3  | Placement of Lockbox Without Written Authority| 1st Violation - $100 Fine         | 13.5                   | Yes            | Yes        |
|        |                                               | 2nd Violation - $200 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $500 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 5.1.4  | Unauthorized Entrance into a Listed Property  | 1st Violation - $100 Fine         | 13.2.1                 | Yes            | Yes        |
|        | (i.e., Failure to Follow the Showing          | 2nd Violation - $200 Fine         |                        |                |            |
|        | Instructions)                                 | 3rd Violation - $500 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 5.1.5  | Failure to Remove Lockbox within 1 day after  | 1st Violation - $100 Fine         | 13.10                  | Yes            | Yes        |
|        | Close of Escrow or expiration/cancellation of | 2nd Violation - $200 Fine         |                        |                |            |
|        | listing.                                      | 3rd Violation - $300 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 5.1.6  | Failure to provide a Kim Users Group approved | 1st Violation - Letter of Warning | 13.2.2                 | No             | Yes        |
|        | iBox lockbox when other lockbox types are     | 2nd Violation - $100              |                        |                |            |
|        | installed.                                    | 3rd Violation - $200              |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 5.1.7  | Failure to comply with property access key    | 1st Violation - Letter of Warning | 13.1                   | No             | Yes        |
|        | rules                                         | 2nd Violation - $50               |                        |                |            |
|        |                                               | 3rd Violation - $100              |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 5.1.8  | Failure by cooperating brokers or buyer's     | 1st Violation - $250              | 13.3                   | Yes            | Yes        |
|        | agent to obtain listing agent permission prior| 2nd Violation - $500 each         |                        |                |            |
|        | to sharing lockbox combinations.              | occurrence thereafter             |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 5.2    | Violation of IDX rules                        | 1st Violation - Letter of Warning | 12.16                  | No             | Yes        |
|        |                                               | 2nd Violation - $250 Fine         |                        |                |            |
|        |                                               | 3rd Violation - Loss of IDX       |                        |                |            |
|        |                                               | Privileges 4-Loss of MLS          |                        |                |            |
|        |                                               | Privileges                        |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 5.3    | Advertising of Listing Filed with the MLS     | 1st Violation - Letter of Warning | 12.8                   | No             | Yes        |
|        | (Outside the scope of IDX)                    | 2nd Violation - $200 Fine         |                        |                |            |
|        |                                               | 3rd Violation - $300 Fine         |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 5.4    | Failure to Provide Adequate Informational     | 1st Violation - Letter of Warning | 12.9                   | No             | Yes        |
|        | Notice on Print or Non-Print Forms of         | 2nd Violation - $100 Fine         |                        |                |            |
|        | Advertising or Other Forms of Public          | 3rd Violation - $200 Fine         |                        |                |            |
|        | Representations                               |                                   |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 5.5    | Non-Completion of Any Required MLS Orientation| 1st Violation - Letter of Warning | 4.1.1, 4.1.2, 4.2.1,   | No             | Yes        |
|        | Program Within 60 Days                        | 2nd Violation - $100 Fine         | 4.2.2                  |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 5.6    | Failure to Pay Any MLS Fees, Fines or Charges | Suspension Until Paid             | 17.1                   | No             | Yes        |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
| 5.7    | Violation of VOW rules                        | 1st Violation - Letter of Warning | 12.19                  | No             | Yes        |
|        |                                               | 2nd Violation - $250 Fine         |                        |                |            |
|        |                                               | 3rd Violation - Loss of IDX       |                        |                |            |
|        |                                               | Privileges 4- Loss of MLS         |                        |                |            |
|        |                                               | Privileges                        |                        |                |            |
+--------+-----------------------------------------------+-----------------------------------+------------------------+----------------+------------+
