APPENDIX B - DEFINITION OF RESIDENTIAL PROPERTY TYPES/CLASS
===========================================================

7.3.1 Residential
~~~~~~~~~~~~~~~~~

**Property Class**

a. **Single Family Detached**

   A single-unit residential property not attached to another living unit. Properties with party or common walls are not permitted in this category.

b. **Apt-Condo**

   An individual living unit contained within a larger building or group of buildings. Typically have shared responsibility for maintenance of the property. Includes co-operatives.

c. **Townhome**

   Typically attached individual living units with no other units above or below. Category does include patio homes. Usually have shared responsibility for maintenance of the property.

d. **Duet Home**

   An attached property with only two residences in the structure, usually side by side.

e. **Loft-Condo**

   An individual living unit often contained within a larger building or group of buildings, usually having high ceilings and modern architectural features. Often found in neighborhoods where live/work is permitted or required.

f. **Manufactured**

   A detached individual residential unit that has been transported to the site in a finished or partially finished state and is supported by a full foundation.

g. **Floating Home**

   An individual living unit floating on water without a means of self-propulsion and connected to public sewer and water systems. Has been assigned an APN. If in the San Francisco Bay Area, is recognized by the Bay Conservation and Development Commission (BCDC).

h. **Tenants in Common (TIC)**

   Undivided interest in a multi-unit building where rights to the use of individual units are marketed separately. Typically have shared responsibility for maintenance of the property.

i. **Lease Rental**

   A residential property available to rent or lease.

7.3.2 Residential Income
~~~~~~~~~~~~~~~~~~~~~~~~

Two or more legally permitted living units on a single or adjacent lots being marketed as a single property. Five or more units can be cross-classed as commercial.

7.3.3 Mobile Homes
~~~~~~~~~~~~~~~~~~

Independent living units that can or could be moved; equipped, or originally equipped, with axles and wheels. Only those that may be sold by real estate licensees. Typically licensed as a vehicle, but may also be real property in some cases. Must have a DOH number prior to listing on the MLS.

7.3.4 Lots & Land
~~~~~~~~~~~~~~~~~

Includes:

- Residential undeveloped land or under-developed parcels being marketed primarily for the value of the land.


- Commercial undeveloped land or under-developed parcels being marketed primarily for the value of the land.


- Agricultural-Ranches and farms

