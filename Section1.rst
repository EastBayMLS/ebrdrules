1. AUTHORITY
============

East Bay Regional Data, Inc. shall maintain for the use of licensed real estate 
brokers and salespersons and licensed or certified Appraisers, a Multiple Listing Service (hereinafter 
also referred to as “MLS” or “service”), which shall be subject to the bylaws of East Bay Regional 
Data, Inc. (hereinafter also referred to as “EBRDI”) and such rules and regulations as may be 
hereinafter adopted.
