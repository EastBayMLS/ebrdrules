10. REPORTING STATUS CHANGES AND OTHER INFORMATION TO THE MLS
=============================================================

10.1 Statuses
-------------
**New**
   Initial input, no accepted offer, remains in status for 7 days after which it becomes Active

**New REO**
   Initial input, no accepted offer, remains in status for 7 days after which it becomes Active, Bank owned listing

**New Short Sale**
   Initial input, no accepted offer, remains in status for 7 days after which it becomes Active, Short Sale listing

**Active**
   No accepted offer

**Active Contingent**
   Offer accepted, contingent on sale of buyer’s other property 

**Active REO**
   No accepted offer, Bank Owned Property

**Active Short Sale**
   No Accepted offer, Potential Short Sale Property

**Back on Market**
   Listing changed status from off-market to Active, remains in status for 7 days after which it becomes Active

**Back on Market REO**
   Listing changed status from off-market to Active, remains in status for 7 days after which it becomes Active REO, Bank Owned Property

**Back on Market Short Sale**
   Listing changed status from off-market to active, remains in status for 7 days afterwhich it becomes Active Short Sale, Potential Short Sale Property

**Price Change**
   Listing Agent modified list price, no accepted offer, remains in status for 7 days after which it becomes Active

**Price Change REO**
   Listing Agent modified list price, no accepted offer, remains in status for 7 days bank owned listing

**Price Change Short Sale**
   Listing Agent modified list price, no accepted offer, remains in status for 7 days short sale listing

**Pending**
   Offer accepted, not contingent on sale of buyer's other property and expected to close 

**Pending Show for Backups**
   Offer accepted but backup offers welcome Pending Subject to Lender Approval–Seller accepted offer, waiting for lender approval

**Pending REO**
   Offer accepted by seller (bank)

**Pending  Court  Confirmation**
   Initial  bid  accepted  but  subject  to  overbid  at  court confirmation hearing

**Pending  Show  Backups  REO**
   Offer  accepted  but  backup  offers  welcome,  bank  owned listing 

**Pending Show Backups Short Sale**
   Offer accepted but backup offers welcome, short sale listing

**Temporarily Withdrawn**
   Listing agreement still in effect but property is temporarily off market 

**Canceled**
   Listing agreement canceled

**Expired**
   Listing agreement expired

**Sold**
   Escrow closed

**Sold REO**
   Escrow closed as Bank owned property

**Sold Short Sale**
   Escrow closed as Short Sale

10.2 Reporting of Sales
-----------------------
Listings  with  accepted  offers  shall  be  reported  to  the  MLS  or input into the MLS database as "pending" within 1 day after of the acceptance, by the listing broker unless the negotiations were carried on under Section 9.1 (a) or (b), in which case, the cooperating broker shall notify the listing broker of the “pending” status within 1 day after acceptance, whereby the listing broker shall then report or input the status change to the MLS within 1 day after receiving notice from the cooperating broker. The listing shall be published on the  MLS as pending with no price or terms prior to the final closing. Upon final closing, the listing broker shall report or input the listing  in  the  MLS  as  "sold"  within 1  day after  the  final  closing  date  unless  the  negotiations  were carried on under Section 9.1 (a) or (b), in which case, the cooperating broker shall notify the listing broker of the “sold” status and selling price within 1  day after  the  final  closing  date,  whereby  the listing broker shall then report or input the status change and selling price within 1 day after receiving notice from the cooperating broker. Listings that were not input into the MLS as a result of the seller’s instructions may be input into the MLS "sold" data at the listing broker’s option. If a listing is entered for comparable purposes only, then “For Comp Purposes Only” shall appear in the first line of confidential remarks.

*For violation of this section, see Appendix A, Citable Infractions, 2.3, Status Changes Not Reported by Deadline and 4.1, Misuse of Remarks.*

10.3 Removal of Listings for Refusal/Failure to Timely Report Status Changes
----------------------------------------------------------------------------
The MLS is authorized to remove any listing from the MLS compilation of current listings where the participant or subscriber has refused or failed to timely report status changes.  Prior to the removal of any listing from the MLS, the Participant and/or subscriber shall be advised of the intended removal so the participant and/or subscriber can advise his or her clients(s).

10.4 Reporting Cancellation of Pending Sale
-------------------------------------------
The Listing Broker shall report to the service within 1 day after the cancellation of any pending sale and the listing shall be reinstated immediately as long as there is still a valid listing.

*For violation of this section, see Appendix A, Citable Infractions, 2.3, Status Changes Not Reported by Deadline.*

10.5 Refusal to Sell
--------------------
If the seller of any listed property filed with the service  refuses to accept  a  written  offer  satisfying,  the  terms  and  conditions  stated  in  the  listing,  such  fact  shall  be transmitted immediately to the service and to all Participants and Subscribers.
