11. OWNERSHIP OF MLS COMPILATIONS AND COPYRIGHTS
================================================

11.1 MLS Compilation Defined
----------------------------
The term “MLS compilation” includes, but is not limited to, the MLS computer database, all printouts of data from the MLS computer database, and all data and  content  therein,  including  but  not  limited  to  photographs,  images  (including  maps),  graphics, audio  and  video  recordings,  virtual  tours,  drawings,  descriptions,  remarks,  narratives,  pricing information, statistics and other details or information related to listed property, all printouts of data and content from the MLS computer database, and all MLS publications. The MLS Compilation is protected by all applicable intellectual property laws.


11.2 Active Listing MLS Compilation Defined
-------------------------------------------
“Active listing MLS compilation” shall mean  that  portion  of  the  MLS  compilation,  which  includes  listings  currently  for  sale  and  all  other indexes and other information relating to the current listing information approved for distribution by the MLS.

11.3 Comparable Data MLS Compilationg Defined
---------------------------------------------
“Comparable  data  MLS compilation” shall mean that portions of the MLS compilation that includes the off market data, sold and  appraisal  information  regarding  properties  that  are  not  currently  for  sale  and  all  indexes  and information relating to the sold information compilation approved for distribution by the MLS.

11.4 Authority to Put Listings in MLS Compilation
-------------------------------------------------
“Comparable  data  MLS compilation” shall mean that portions of the MLS compilation that includes the off market data, sold and  appraisal  information  regarding  properties  that  are  not  currently  for  sale  and  all  indexes  and information relating to the sold information compilation approved for distribution by the MLS.

11.5 Photographs on the MLS
---------------------------
By submitting photographs to the MLS, the participant and/or subscriber represents and warrants that it either owns the right to reproduce and display such photographs or has procured such rights from the appropriate party, and has the authority to grant and hereby grants the MLS and the other Participants and Subscribers the right to reproduce and display the photographs in accordance with these rules and regulations.  Except by the MLS for purposes of protecting its rights under Section 11.6, branding of photographs, virtual tours or any photographic representation  with  any  information  or  additional  images,  including  but  not  limited  to  photos displaying “for sale” signs posted on the property, is prohibited.

a) At  least  one  (1)  photo  or  graphic  image  of  the  front  exterior  of  the  property accurately displaying the listed property (except where sellers expressly direct in writing that photographs of their property not appear in MLS compilations) is required to be posted on the MLS upon submission of the listing in all categories other than business opportunity and Lots and Land.

b) Use  of  photographs  by  a  subsequent  listing  agent  requires  prior  written authorization  from  the  originating  listing  agent  or  appropriate  party  with  the  legal  right  to reproduce and display such photographs.

*For violation of this section, see Appendix A, Citable Infractions, 3.1, Submission of Listing That Do Not Satisfy the Requirements of the MLS Rules.*

11.6 Copyright Ownership
------------------------
All  right,  title,  and  interest  in  each  copy  of  every  MLS compilation created and copyrighted by EBRDI, and in the copyrights therein, shall at all times remain vested in EBRDI.  The MLS shall have the right to license such compilations or portions thereof to any entity pursuant to terms agreed upon by the EBRDI Board of Directors.

11.7 Access to MLS Compilations
-------------------------------
Each Participant and Subscriber shall have the right and license to access the Active Listing and Comparable Data MLS Compilations in accordance and subject to all restrictions contained in these rules.  Participants and Subscribers shall acquire by such license only the right to individually use the MLS compilations, and only for purposes permitted by these  rules.    Clerical  Users  may  have  access  to  the  information  solely  under  the  direction  and supervision of the Participant or Subscriber.  Clerical Users may not provide any MLS compilation or information to persons other than the Participant or the Subscriber under whom the Clerical User is registered.

11.8 Database Preservation
--------------------------
No data may be removed from the MLS compilation other than  by  the  service. Although  a  listing  may  be  removed  from  display  in  the  MLS  compilation  of current listing information, all data submitted to the MLS will remain in the database for historical and other purposes approved by the service.

11.9 Removal of and Responsibility for Content
----------------------------------------------
The  MLS  has  the  right,  but  not  the obligation, to reject, pull down, restrict publication of, access to or availability of content the MLS in good faith considers to be obscene, lewd, lascivious, filthy, excessively violent, harassing, unlawful or otherwise objectionable. Participants and Subscribers remain solely responsible and liable for the content they provide. In no case will any monitoring or removal of Participants’ or Subscribers’ content by the MLS make it responsible or liable for such content.

11.10 Indeminification; Limitation of Liability
-----------------------------------------------
Participant and Subscriber shall indemnify and hold harmless the service for any claims, costs, damage or losses, including reasonable attorney fees  and  court  costs,  incurred  by  the  MLS  resulting  from  or  arising  out  of  any  content  Participant and/or Subscriber submit to or in any way wrongfully reproduce from the Service. In no event will the MLS be liable to any MLS Participant, Subscriber or any other party for any indirect, special or consequential damages arising out of any information published in the MLS and all other damages shall be limited to an amount not to exceed the MLS fees paid by the listing broker.  
