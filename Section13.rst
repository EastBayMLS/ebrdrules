13. LOCKBOXES
=============

13.1 Eligibility for Lockboxes Privileges
-----------------------------------------
MLS Participants and Subscribers are eligible for  lockbox  privileges  if  they  otherwise  qualify  under  this  section.    Keys/Boxes  shall  be  issued through one of EBRDIs  Shareholder  Operated Service Centers.  Clerical  Users are not  eligible for lockbox  privileges.    MLS  Participants  and  Subscribers  shall  be  eligible  to  hold  a  lockbox  key provided:

a. The key holder signs an agreement with an organization (hereafter “Organization”) that is a  member  of  the  Bay  Area  LENI/KIM  Users  Group.    The  agreement  shall  include  and bind the Participant or Subscriber to all of the provisions of this section 13.

b. The  Participant  to  which  the  key  holder  is  licensed  cosigns  the  agreement  with the Organization.

c. The key holder continues to comply with all MLS rules relating to lockbox keys.

d. The  key  holder  and  Participant  to  whom  the  key  holder  is  licensed  remain  eligible  for MLS services.


13.2 Key  Use  and  Service
----------------------------   
Keys  may  not  be  used  under  any  circumstances  by  anyone other than the key holder including but not limited to, lending, borrowing or sharing keys with others.  The  AOR  is  not  obligated  to  provide  service  on  keys  or  lockboxes  to  individuals  who  are  not  the registered lessee or owner of the component.  Keys may only be used for the purpose of facilitating the sale or lease of a listed property.

13.2.1 Use of Lockbox Contents
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Participants and Subscribers shall at all times follow the  showing  instructions  published  in  the  MLS.  Participants  and  Subscribers  shall  not  remove contents  of  the  lockbox  for  purposes  other  than  showing  the  home  and  shall  promptly  return  the contents to the lockbox upon exiting the property.  Participants and Subscribers shall keep lockbox contents in their possession at all times after removal from the lockbox. The lockbox and/or contents shall not be removed from the property site without prior consent from the listing agent.

*For violation of this section, see Appendix A, Citable Infractions, 5.1., Showings and Access.*

13.2.2  Lockbox Requirements
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
If any lockbox or other device giving access to On Market listed  property  for real  estate  professionals  and/or  service  providers  is  authorized  by  the  seller and/or occupant and is placed on or present on property listed through the Service, such lockbox or device must be one that is approved by the MLS where the listing has been submitted.  The authorized lockboxes sold by, leased by or otherwise offered through the local Association or MLS where the listing  is  submitted  have  been  approved  by  the  MLS (Kim  User  Group  approved Supra  BT LE lockbox). Unless expressly indicated otherwise by the MLS, for any other lockbox or device to be considered “MLS-approved,” use of it must provide reasonable, timely access to listed property such that:

(1) it allows all participants and subscribers timely access to listed property by reliance solely on data submitted to and residing on the MLS;

(2) complete,  accurate  and  stand-alone instructions  are  provided  for  accessing  the  listed property in the appropriate agent section on the Service; and

(3) it ensures that the lockbox or device will provide reasonable access to listed property with any  information,  code  or  key  needed  to access  the  contents  of  the  lockbox  or  device  to be made available or access to the property otherwise scheduled within four (4) Four hours of initial contact in the event the lockbox or device requires the participating member to obtain additional information to enable access (ex: “call listing agent for entry code”) with said 4-hour response obligation in effect every day from 8am to 6pm. The  MLS  reserves  the  right  to  require  that  the  device  be  submitted  in  advance  for approval. The MLS also may revoke the approval and/or subject the participant to discipline if the device  is  used  in  a  manner  that  fails  to  continue  to  satisfy  this  requirement.  Failure  to  provide reasonable and timely access as required by this section will subject the listing agent to discipline and potential fines. More than one lockbox or access device may be used on a property as long as one of them  is  MLS-approved (Kim  User  Group  approved Supra  BT  LE lockbox) where  the  listing  is submitted.

**For violation of this section, see Appendix A, Citable Infractions, 5.1 Showings and Access**

13.3 Accountability
-------------------
Key holders must account for keys at the time of any inventory conducted  by  the  AOR  or  at  any  time  requested  by  the  AOR.    Key  holders  who  cease  to participate  or  subscribe  to  the MLS  shall  return  all  keys(s)  in  their  possession  to  the  AOR.  Failure to return a key(s) will subject the key holder and/or the key holder’s Participant to fines and penalties and to being responsible for all costs incurred by the AOR to secure the lockbox key system as a result of the failure to return the key(s).

13.4 Deemed  Unaccountable
--------------------------
Keys  shall  be  deemed  unaccounted  for  if  a  key  holder refuses or is unable to demonstrate that the key is within the key holder’s physical control.  For violation of this section, see Appendix A, Citable Infractions, 5.1, Showings and Access.

13.5 Written  Authority
-----------------------
Participants  and  Subscribers  shall  not  place  a  lockbox  on  a property  without  written  authority  from  the  seller  and  occupant  if  other  than  the  seller.  Inclusions  in  MLS  compilations  cannot  be  required  as  a  condition  of  placing  lockboxes  on listed property.

13.6 Unaccountable Keys
-----------------------
Key holders and Participants cosigning with a key holder shall immediately report lost, stolen or otherwise unaccountable keys to the AOR.

13.7 Deposits
-------------
All key holders may be required to give the AOR deposits in accordance with  the  deposit  schedule  adopted  by  the  MLS  Committee  and  approved  by  the  Board  of Directors.  Key holders shall forfeit the deposits if the key is lost, stolen or unaccounted for.  Key holders shall not be entitled to any interest on their deposits.  The AOR is not obligated to refund deposits to individuals who are not the registered lessee or owner of the key.

13.8 Rules Violations
---------------------
Failure to abide by rules relating to lockboxes as set forth in this section or failure to abide by the key lease agreement may result in discipline as provided in sections  14  and  of  these  rules,  in  addition  to  loss  of  or  restriction  on  all  lockbox  and  key privileges.

13.9 Right to Limit Access
--------------------------
The Organization reserves the right to refuse to issue a key or limit  access  to  lockboxes  if,  in  its  sole  discretion,  it  determines  the  security  of  the  system would be compromised by issuing such keys or granting access to lockboxes.

13.10 Removal  of  Lockbox
--------------------------
Upon  Sale  of  the  property,  the  lockbox  must  be  removed within 1 day after the close of escrow or expiration/cancellation of the listing.   

*For violation of this section, see Appendix A, Citable Infractions, 5.1, Showings and Access.*
