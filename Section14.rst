14. VIOLATIONS OF RULES AND REGULATIONS
=======================================

14.1 Grounds for Disciplinary Action and Sanctions
--------------------------------------------------

After a hearing by a hearing panel, which shall be assigned to an EBRDI Shareholder’s Association as provided in the California Code of Ethics and Arbitration Manual, the EBRDI  Board of Directors may take disciplinary  action and impose sanctions against any MLS Participant and Subscriber:

a. For violation of any MLS rule;

b. On the Participant’s or Subscriber’s being convicted, adjudged, or otherwise recorded as guilty by a final judgment of any court of competent jurisdiction of (1) a felony, or (2) a crime  involving  moral  turpitude,  or  (3)  on  a  determination  by  any  court  of  competent jurisdiction,  or  official  of  the  State  of  California  authorized  to make  the  determination, that the Participant or Subscriber violated a provision of the California Real Estate Law or a Regulation of the Real Estate Commissioner or the laws relating to Appraisers or a regulation of the Office of Real Estate Appraisers.

c. For any violation of subsection (a) by any person, including but not limited to a Clerical User  or  a  salesperson,  who  is  not  a  Participant  or  Subscriber  but  is  employed  by  or affiliated with such Participant or Subscriber and was providing real estate related services within the scope of the Participant’s or Subscriber’s license.  Lack of knowledge by the Participant or Subscriber of such salesperson’s conduct shall only go to mitigation of discipline imposed.

d. For  any  violation  of  the  N.A.R.  Code  of  Ethics, while  a  member  of  any  Association  of Realtors®.

14.2 Sanctions
--------------

Sanctions or disciplinary action for violation of an MLS Rule may consist of one or more of those specified in the California Code of Ethics and Arbitration Manual.

14.3 Citations
--------------

The  EBRDI  Board  of  Directors,  may  implement  a  schedule  of  fines  for certain MLS rules violations and direct staff to issue citations for the specified MLS rules violations and implement a procedure whereby the Participant and Subscriber receiving the citation may either pay the amount specified on the citation or request a full hearing in accordance with the procedures set forth in the California Code of Ethics and Arbitration Manual.

*See Appendix A, “Citable Infractions” for a summary of known potential infractions. Appendix A may be revised as necessary by the Board of Directors.*

14.4 Fines
----------

All fines will be paid to EBRDI MLS.  EBRDI Shareholder’s Associations may charge up to $500 Administrative Fees to conduct hearings and grievances consistent with the California Code of Ethics and Arbitration Manual.
