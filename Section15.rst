15. PROCEDURES FOR MLS RULES HEARINGS
=====================================

All MLS rules hearings shall be processed in accordance with the California Code of Ethics and  Arbitration  Manual  as  from  time  to  time  amended  which  is  hereby  incorporated  by  reference.  Failure to abide by the procedures shall be a violation of these MLS rules.

