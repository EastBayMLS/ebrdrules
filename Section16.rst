16. ARBITRATION
===============

16.1 Mandatory Arbitration
--------------------------

By becoming and remaining a Participant or Subscriber in the  MLS,  each  Participant  and  Subscriber  agrees  to  submit  disputes  arising  out  of  the  real  estate business  which  also  arises  out  of,  or  is  in  conjunction  with,  any  listing  filed  with  the  MLS  or  any appraisal, to binding arbitration which shall be assigned to an EBRDI Shareholder’s Association with any other Participant or Subscriber of this MLS, or Participants or Subscribers of any other MLS who are  authorized  to  have  access  to  this  MLS  under  section  6  of  these  rules.    The  California  Code  of Ethics shall govern such arbitrations and Arbitration Manual as from time to time amended which is hereby incorporate by reference.  This shall be deemed an arbitration agreement within the meaning of Part 3, Title 9 of the California Code of Civil Procedure.  Failure to submit to arbitration and abide by  the  arbitration  award,  including  but  not  limited  to  timely  payment  of  the  arbitration  award as provided herein shall be a violation of these MLS rules and subjects Participants and Subscribers to possible suspension from the MLS and/or other penalties.

16.1.1 Administration of all Arbitrations & Hearings
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Administration  of  all arbitrations  shall  be  delegated  to  one  of  the  Shareholder  Associations.    Assignment  of arbitration/rules violation responsibility shall be a follows:

a. If respondent is a member of a Shareholder’s Association, responsibility shall be assigned to that Shareholder’s Association.

b. If respondent is an MLS Only Participant, the complaint shall be assigned to the Shareholder Service Center where the respondent receives MLS services.

c. All others shall be assigned as deemed appropriate by EBRDI.

16.2 Other Arbitration Agreements
---------------------------------

Notwithstanding any other provision of these rules, if any Participant or Subscriber enters into an agreement (either before or after a dispute arises) with another Participant or Subscriber to arbitrate a dispute utilizing non-EBRDI facilities, such persons are not bound to arbitrate the dispute covered by such agreement under these rules utilizing EBRDI facilities.

16.3 Arbitration  between  Association  Members
-----------------------------------------------

Notwithstanding  any  other  provision of these rules:

a. If  all  disputants  are  Participants  or  Subscribers  of  EBRDI,  they  shall  arbitrate  in accordance with 16.1.1.

b. If the disputants are Participants or Subscribers of MLS’s other than EBRDI, they shall arbitrate  in  accordance  with  any  applicable  regional  or  shared  professional  standards agreement.    In  the  absence  of  such  an  agreement,  the  disputants  remain  obligated  to arbitrate at the California Association of REALTORS® (“C.A.R.”) in accordance with the C.A.R Inter-Board Arbitration Rules.

16.4 Arbitration Involving Non-Association Members – See 16.1.1 above
---------------------------------------------------------------------

16.5 Same Firm
---------------
Between persons from the same firm shall not be  available  and is not mandated by these rules unless covered by arbitration rules relating to the obligations of Association members to arbitrate.

16.6 Timing
-----------

For purposes of the section 16, the duty to arbitrate shall be determined when facts giving rise to the dispute occurred.  Therefore, a Participant or Subscriber shall have a duty to arbitrate  if  the  person  was  an  MLS  Participant  or  Subscriber  when  facts  giving  rise  to  the  dispute occurred.    Termination  of  MLS  participation  or  subscription shall  not  relieve  the  arbitration  duty under  this  section  for  disputes  that  arose  when  the  person  was  an  MLS  Participant  or  Subscriber.  Requests for arbitration must be filed within one hundred and eighty (180) days after the closing of the transaction, if any, or after the facts constituting the matter could have been known if the exercise of reasonable diligence, whichever is later.
