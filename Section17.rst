17. NONPAYMENT OF MLS FEES
==========================

17.1 Non-payment of MLS Fees
----------------------------

If MLS fees, fines, charges or other amounts owed the MLS  are  not  paid  within  one  month  after  the  due  date,  the  non-paying Participant’s, Subscriber’s and/or clerical user’s MLS services shall be subject to suspension until such outstanding amounts are paid in full.   The MLS  may  suspend MLS services under this section provided the MLS  gives the Participant  and/or  Subscriber  at  least  twenty-(20)  calendar  day’s  prior  notice  of  the  proposed suspension date.  Such notice may be included with the original billing statement for MLS fees, fines or charges or any time thereafter.  In the event the amounts owed remain unpaid for two months after the  due  date,  the  non-paying  Participant  and/or  Subscriber’s  MLS  services  shall  automatically terminate regardless if notice of such termination is given.

17.2 Disputed  Amounts
----------------------

If a Participant  and/or  Subscriber  disputes  the  accuracy  of amount owed, the Participants and/or Subscriber may request a hearing before the Board of Directors.  In order to request such a hearing, the Participant and/or Subscriber must first pay the disputed amount in whole, which may be refunded in whole or part in accordance with the Board of Directors’ determination.  Hearings under this section shall be conducted in accordance with the California Code of Ethics and Arbitration Manual.  In the event the Board of Directors confirms the accuracy of the amount owed, the Participant and/or Subscriber shall also be subject to paying interest at the rate of ten (10%) per annum on such past due amounts.

17.3 Reinstatement
------------------

Any  Participant  and/or  Subscriber whose MLS  services  have  been terminated for non-payment of MLS fees may reapply for participation in the MLS.  However, prior to  being  granted  access,  such  Participant  and/or  Subscriber  must  pay  all  fees  applicable  to  new applicants and all past due amounts owed, including paying interest at the rate of ten (10%) per annum on such past due amounts.
