18. CHANGES IN RULES AND REGULATIONS
====================================

The rules and regulations of the MLS may be amended as specified in the EBRDI By-laws.  Any  changes  to  these  rules  and  regulations which  are  mandated  by  the  National  Association  of REALTORS® shall automatically be incorporated into these rules and regulations and do not require Board of Directors approval.
