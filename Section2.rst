2. PURPOSE
==========

A Multiple Listing Service is a means by which authorized MLS Broker 
Participants establish legal relationships with other Participants 
by making a blanket unilateral contractual offer of compensation
and cooperation to other Broker Participants; by which information 
is accumulated and disseminated to enable authorized Participants 
to prepare appraisals and other valuations of real property; by which
Participants engaging in real estate appraisal contribute to common
databases; and is a facility for the orderly correlation and dissemination 
of listing information among the Participants so that they may better serve
their clients and the public.  Entitlement to compensation is determined by
the cooperating broker’s performance as a procuring cause of the sale or lease.
