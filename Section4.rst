4. PARTICIPATION AND AUTHORIZED ACCESS
======================================

4.1 Participant 
---------------
A Participant is any individual who applies and is accepted by the MLS, 
meets  and  continues  to  meet  all  of  the  following  requirements  of  either  a  Broker  Participant  or  an 
Appraiser Participant as defined below in sections 4.1.1 and 4.1.2.

4.1.1 Broker Participant 
~~~~~~~~~~~~~~~~~~~~~~~~
A Broker  Participant  is  a  Participant  who  meets  all of 
the following requirements:

a. The  individual,  or  corporation  for  whom  the  individual  acts  as  a  broker/officer, holds a valid California real estate broker’s license;

b. The individual is a principal, partner, corporate officer, or branch office manager acting on behalf of a principal;

c. The  individual  or  corporation  for  whom  the  individual  acts  as  a  broker/officer offers and/or accepts compensation in the capacity of a real estate broker;

d. The individual has signed a written agreement to abide by the rules and regulations of the service in force at that time and as from time to time amended;

e. The individual pays all applicable fees; and

f. The  individual  has  completed  an  orientation  program  of  no  more  than eight  (8) classroom hours within sixty (60) days after access is provided.


.. note::  Mere  possession  of  a  broker's  license  is  not  sufficient  to  qualify  for  MLS  participation.  Rather,  the requirement that an individual or firm “offers and/or accepts compensation” means that the Participant actively endeavors during the  operation of its real estate  business to list real property of the  type listed  on the MLS and/or  to  accept  offers  of  cooperation  and  compensation  made  by  listing  brokers  or  agents  in  the  MLS. “Actively” means on a continual and on-going  basis  during  the  operation  of  the  Participant's  real  estate business. The “actively” requirement is not intended to preclude MLS participation by a Participant or potential Participant that operates a real estate business on a part time, seasonal, or similarly time-limited basis or that has its business interrupted by periods of relative inactivity occasioned by market conditions. Similarly, the requirement is  not intended to deny MLS participation to a  Participant or potential Participant who has not achieved a minimum number of transactions despite good faith efforts. Nor is it intended to permit an MLS to deny participation based on the level of service provided by the Participant or potential Participant as long as the level of service satisfies state law. 

The key is that the Participant or potential Participant actively endeavors to make or accept offers of 
cooperation  and  compensation  with  respect  to  properties  of  the  type  that  are  listed  on  the  MLS  in 
which participation is sought. This requirement does not permit an MLS to deny participation to a Participant or potential Participant that operates a Virtual Office Website (“VOW”) [See Rule No. 
12.19]  (including  a  VOW  that  the  Participant  uses  to  refer  customers  to  other  Participants)  if  the 
Participant  or  potential  Participant  actively  endeavors  to  make  or  accept  offers  of  cooperation  and 
compensation.  An  MLS  may  evaluate  whether  a  Participant  or  potential  Participant  “actively endeavors during the operation of its real estate business” to “offer and/or accept compensation” only if the MLS has a reasonable basis to believe that the Participant or potential Participant is in fact not doing so.  

The membership requirement shall be applied on a nondiscriminatory manner to all Participants and 
potential Participants.  

*For violation of this section, see Appendix A, Citable Infractions, Section 1.1 Use of MLS System by 
Unauthorized Party and.5.5, Non-Completion of Any Required Orientation Program within 60 Days*

4.1.2 Appraiser Participant
~~~~~~~~~~~~~~~~~~~~~~~~~~~
An Appraiser Participant is a Participant who meets the following requirements:

a. The individual holds a valid California Appraisers certification or license; and

b. The individual is a principal, partner, corporate officer, or branch office manager acting on behalf of a principal; and

c. The individual has signed a written agreement to abide by the rules and regulations of the service in force at that time and as from time to time amended;

d. The individual pays all applicable fees; and

e. The  individual  has  completed  an  orientation  program  of  no  more  than  eight  (8) classroom hours within sixty (60) days after access is provided.


4.1.3 Redundant Participant Qualifications
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Participant type (Broker or Appraiser)  must  be  selected  during  application  for  participation.  Participant with  both  a  California  Real  Estate  Broker’s  license  and  a  California Appraiser’s certification or license must join as a “Broker Participant” to be listing broker under Section 4.6 or a cooperating broker or selling broker under section 4.7.

*For violation of this section, see Appendix A, Citable Infractions, Section 1.1 Use of MLS System by Unauthorized Party and.5.5, Non-Completion of Any Required Orientation Program within 60 Days*

4.2 Subscriber
--------------
A Subscriber is an individual who applies and is accepted by the MLS, meets the requirements of either a real estate Subscriber or appraiser Subscriber as defined below in sections 4.2.1 and 4.2.2:

4.2.1 Real Estate Subscriber
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
A real estate Subscriber is a Subscriber who meets all of the following requirements:

a. The  individual  holds  a  valid  California  real  estate  Salesperson’s  or  Broker’s license;

b. The  individual  is  employed  by  or  affiliated  as  an  independent  contractor  with  a Broker Participant;

c. The individual has signed a written agreement to abide by the rules and regulations of the service in force at that time and as from time to time amended;

d. The individual pays all applicable MLS fees; and 

e. The individual has completed any required orientation program of no more than eight (8) classroom hours within sixty (60) days after access is provided.

*For violation of this section, see Appendix A, Citable Infractions, Section 1.1 Use of MLS System by Unauthorized Party and 5.5, Non-Completion of Any Required Orientation Program within 60 Days*

4.2.2 Appraiser Subscriber
~~~~~~~~~~~~~~~~~~~~~~~~~~
An Appraiser Subscriber is a Subscriber who meets all of the following requirements:

a. The individual hold a valid California real estate appraisers certification or license;

b. The individual is employed by or affiliated as an independent contractor with an Appraiser Participant;

c. The individual has signed a written agreement to abide by the rules and regulations of the service in force at that time and as from time to time amended;

d. The individual pays all applicable MLS fees; and

e. The individual has completed any required orientation program of no more than eight (8) classroom hours within sixty (60) days after access is provided.


4.2.3 Redundant Subscriber Qualifications
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Subscriber  type  real  estate  or appraiser must correlate  to the participant type.  A subscriber who is both a California Real Estate licensee and California certified or licensed appraiser must join as a R.E. Subscriber unless their employing or affiliated participant is an Appraiser participant.

*For violation of this section, see Appendix A, Citable Infractions, Section 1.1 Use of MLS System by Unauthorized Party and 5.5, Non-Completion of Any Required Orientation Program within 60 Days.*

4.3 Clerical Users
------------------
Individuals (whether licensed or unlicensed) under the direct supervision of an MLS Participant or Subscriber that perform only administrative and clerical tasks that do not require a real estate license or an appraisers certificate or license. Clerical Users may join the MLS through their employing Participant or Subscriber.  The Participant shall be responsible for 
the conduct of the Clerical User. Clerical Users shall be linked in the system to at least one Participant. They may also be linked to a particular Subscriber. Each Participant and Subscriber shall provide the MLS with a list of all Clerical Users employed by or affiliated as independent contractors with the Participant or Subscriber and shall immediately notify the MLS of any changes, additions or deletions from the list. Clerical Users shall also be subject to the following requirements: 

a. Clerical Users are given a unique passcode;

b. Clerical Users must have any fees paid in full;

c. Participant or Subscriber linked to the Clerical User may be fined, disciplined or terminated for Clerical User’s misconduct;

d. Clerical Users shall sign a written agreement to abide by the rules and regulations of the MLS; and

e. Clerical  Users  may  complete  any  required  orientation  program  of  no  more  than eight (8) classroom hours within sixty (60) days after access has been provided.


4.4 Notification of Licensees
-----------------------------
Each Participant shall provide the MLS with a list of all real  estate  licensees  or  certified  or  licensed  Appraisers  employed  by  or  affiliated  as  independent contractors  with  such  Participant  or  with  such  Participant’s firm and shall immediately notify the MLS of any changes, additions or deletions from the list.  This list shall include any licensees under any broker associate affiliated with the Participant.

*For violation of this section, see Appendix A, Citable Infractions, 1.1, Use of MLS System by Unauthorized Party.*

4.5 Participation Not Transferable
----------------------------------
Participation in the MLS is on an individual basis and may not be transferred or sold to any corporation, firm or other individual.  Any reimbursement of MLS  fees  is  a  matter  of  negotiation  between  those  transferring  the  business  or  determined  by internal  contract  arrangement  within  the  firm.    However,  providing  the  first  Participant  consents, EBRDI  shall  allow  a  firm  to  designate  a  different  person  as  a  Participant  within  the  firm  without additional  initial  participation  fees. EBRDI may charge  an  administrative  fee  for  this  service  of reassigning Participants within a firm.

4.6 Listing Broker Defined
--------------------------
For  purposes  of  these  MLS  rules,  a  Listing  Broker  is  a Broker Participant who is also a listing agent as defined in Civil Code § 1086(f) who has obtained a written listing agreement by which the broker has been authorized to act as an agent to sell or lease the property or to find or obtain a buyer or lessee.  Whenever these rules refer to the Listing Broker, the term shall include the real estate Subscriber or a licensee acting for the Listing Broker but shall not relieve the Listing Broker of responsibility for the act or rule specified.

4.7 Cooperating Broker or Selling Broker Defined
------------------------------------------------
For purposes of these MLS rules, a cooperating broker or selling broker is a Broker Participant who is also a selling agent as defined in Civil Code § 1086 who acts in cooperation with a Listing Broker to accept the offer of compensation and/or sub-agency to find or obtain a buyer or lessee.  The cooperating broker or selling broker may be the agent of the buyer or, if sub-agency is offered and accepted, may  be the agent of the seller.  Whenever these rules refer to the cooperating broker or selling broker, the term shall include the R.E. Subscriber of licensee acting for the cooperating  or selling broker but shall not relieve that Broker Participant of responsibility for the act or rule specified.

4.8 Appraiser Defined
---------------------
For purposes of these MLS rules, an Appraiser is an Appraiser Participant,  Appraiser  Subscriber,  or  licensed  or  certified  Appraiser  acting  for  the  Appraiser Participant or Appraiser Subscriber.  Whenever these rules refer to the Appraiser, the term shall also include the Appraiser Subscriber or a licensed or certified Appraiser employed by or affiliated as an independent contractor with the firm that employs the Appraiser but shall not relieve that Appraiser Participant of responsibility for the act or rule specified.

4.9 Denied Application
----------------------
In the event an application for participation in the MLS is rejected by the MLS, the applicant, and his or her broker, if applicable, will be promptly notified in writing of the reason for the rejection. The broker shall have the right to respond in writing, and to request a hearing in accordance with the *California Code of Ethic and Arbitration Manual*.

4.10 Interim Training
---------------------
Participants and Subscribers may be required, at the discretion of the  MLS,  to  complete  additional  training  of  not  more  than  four  (4)  classroom  hours  in  any  twelve (12) month period when deemed necessary by the MLS to familiarize Participants and Subscribers with  system  changes  or  enhancement  and/or  changes  to  MLS  rules  or  policies.  Participants  and Subscribers must be given the opportunity to complete any mandated additional training remotely.
