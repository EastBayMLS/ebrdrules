5. MLS FEES AND CHARGES
=======================

5.1 Service Fees and Charges
---------------------------
The EBRDI Board  of Directors shall set the following service fees and charges:

5.1.1 Initial Participation Fee and/or Application Fee
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Applicants for MLS services may be assessed initial participation and/ or application fee. 

5.1.2 Recurring Participation Fee
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The recurring participation fee of each Broker Participant shall be an amount times the total number of (1) the Participant plus (2) the number of  salespersons  who  have  access  to  and  use  of  the  MLS,  whether  licensed  as  brokers  or salespersons,  who  are  employed  by  or  affiliated  as  independent  contractors  with  such Participant or the Participant’s firm.  If more than one principal broker in the firm elects to be a  Participant,  the  number  of  salespersons  in  the  firm  will  be  used  once  in  calculating  the recurring  participation  fees.    A  Broker  Participant  is  not  obligated  to  pay  recurring participation fees or other MLS fees and charges for real estate licensees affiliated with the Participant or the Participant’s firm if such licensees work out of a branch office of the Participant or the Participant’s firm that does not participate in or otherwise use the MLS.

The recurring participation fee of each Appraiser Participant shall be an amount times the total number of (1) the Appraiser Participant plus (2) the number of Appraisers who have access to and use of the MLS, who are employed by or affiliated as independent contractors with such Participant or the Participant’s firm.  If more than one principal Appraiser in the same firm elects to be a Participant, the number of Appraisers in the company will only be used  once  in  calculating  the  recurring  participation  fees.    An  Appraiser  Participant  is  not obligated  to  pay  recurring  participation  fees  or  other  MLS  fees  and  charges  for  licensed  or certified Appraisers affiliated with the Participant or the Participant’s firm if such Appraisers work out of a branch office of the Participant or the Participant’s firm that does not participate in or otherwise use the MLS.

5.1.3 Listing Fee
~~~~~~~~~~~~~~~~~
A  Broker  Participant  shall  pay  a  listing  fee  for  each  listing submitted to the MLS staff for input.

5.1.4 Book Fee
~~~~~~~~~~~~~~
If applicable, the Participant shall be responsible for book fees for each MLS book the Participant wishes to lease.  The Participant may not obtain more MLS books than the total number of Subscribers affiliated with the Participant.

5.1.5 Computer Access Fees
~~~~~~~~~~~~~~~~~~~~~~~~~~
If applicable, the recurring computer access fee for each Participant shall be an amount times the total number of Subscribers and salespersons licensed or certified as Appraisers, brokers or salesperson, who are employed by or affiliated as independent contractors with such Participant.

5.1.6 Certification of Non-use
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Participants may be relieved from payment under sections  5.1.2  and  5.1.5  hereunder  by  certifying  in  writing  to  the  MLS  that  a  licensed  or certified  person  in  the  office  is  engaged  solely  in  activities  that  do  not  require  a  real  estate license or certification (clerical, etc.), or that the real estate licensee or licensed or certified Appraiser will not use the MLS or MLS compilation in any way.   In the event a real estate licensee or Appraiser is found in violation of the non-use certification, the Participant shall be subject to fees dating back to the date of the certification.  The Participant and Subscriber may also be subject to any other sanction imposed for violation of MLS rules including, but not limited  to,  a  citation  and  suspension  or  termination of  ultimately  losing  participation  rights and access to the service.

*For violation of this section, see Appendix A, Citable Infractions, 1.1, Use of MLS System by Unauthorized Party.*

5.1.7 Clerical Users
~~~~~~~~~~~~~~~~~~~~
Clerical  users  may  be  assessed  application  fees, computer access  fees  and  other  fees.    The  Participant  for  the  clerical  user  shall  be  responsible  for  all such fees.

5.1.8 Other Fees
~~~~~~~~~~~~~~~~
Other fees that are reasonably related to the operation of the MLS may be adopted.

5.2 Responsibility for Fees
---------------------------
In the event the MLS allows for direct billing or payment by a Subscriber for fees under these rules, such fees shall be the exclusive obligation of that Subscriber regardless  of  whether  such  Subscriber  becomes  affiliated  with  a  different  Participant. If the MLS does not allow  for  direct  billing  or  payment  by  a  Subscriber  for  MLS  fees,  such  fees  shall  be  the responsibility of the Participant with whom the Subscriber was affiliated with at the time the MLS fees were incurred.  This section does not preclude in any way the ability of Participants to pursue reimbursement  of  MLS  fees  from  current  or  past  Subscribers  or  to  establish  agreements  with Subscribers regarding payment or reimbursement of MLS fees.
