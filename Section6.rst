6. REGIONAL AND RECIPROCAL AGREEMENTS
=====================================

The  Board  of  Directors  of EBRDI  may  approve  and  enter  into  reciprocal  or  regional agreements  with  other  Associations  of  REALTORS® or  MLS  Corporations  owned  solely  by Associations  of  REALTORS®  to  allow  the  other  MLS  Participants  and  Subscribers  access  to  the service in exchange for comparable benefits to the Participants and Subscribers of this service.  In the event of such agreements, the Participants and subscribers agree to abide by the respective rules of the other MLSs receiving and publishing a listing pursuant to such agreements and to abide by such rules when accessing the other MLSs database.
