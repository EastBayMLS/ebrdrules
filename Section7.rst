7. LISTING PROCEDURE
====================

7.1 Listings Subject to Rules and Regulations of the Service
------------------------------------------------------------
Any listing filed with the service is subject to the rules and regulations of the service.

7.2 Types of Listings: Responsibility for Classification
--------------------------------------------------------
The  service  shall  accept exclusive right to sell, exclusive agency, open and probate listings as defined in California Civil Code § 1086 et. seq. that satisfy the requirements of these MLS rules.  Exclusive right to sell listings that contain  any  exceptions  whereby  the  owner  need  not  pay  a  commission  if  the  property  is  sold  to particular individuals shall be classified for purposes of these rules as an exclusive right to sell listing, but the Listing Broker shall notify all Participants of the exceptions.  By so classifying a listing, the Listing Broker certifies that the listing falls under the legal classification designated.  It shall be the responsibility  of  the  Broker  Participant  and  real  estate  Subscriber  to  properly  classify  the  type  of listing submitted and, if necessary, to obtain a legal opinion to determine the correct classifications; the MLS shall not have an affirmative responsibility to verify such legal classifications.  The MLS shall have no affirmative responsibility to verify the listing type of any listing filed with the service.  However,  the  MLS  shall  have  the  right  to  have  legal  counsel  make  a  determination  as  to  the classification of the listing type and if the Listing Broker does not reclassify it accordingly, EBRDI shall  have  the  right  to  reject  or  remove  any  such  listing  that  it  determines falsely represents  the classification of a listing.

*For violation of this section, see Appendix A, Citable Infractions, 3.1., Reporting and Accuracy of Information.*

7.2.1 Scope of Service; Limited Services Listings
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Limited  Service  Listings  are listings whereby the Listing Broker, pursuant to the listing agreement, will not provide one, or more, of the following services:

a. provide  cooperating brokers  with  any  additional  information  regarding  the property not already displayed in the MLS but instead gives cooperating brokers authority to contact the seller(s) directly for further information;

b. accept  and  present  to  the  seller(s)  offers  to  purchase  procured  by  cooperating brokers  but  instead  gives  cooperating  brokers  authority  to  present  offers  to purchase directly to the seller(s);

c. advise the seller(s) as to the merits of offers to purchase;

d. assist the seller(s) in developing communicating, or presenting counter-offers; or

e. participate on the seller(s) behalf in negotiations  leading to the sale of the listed property.

Said Limited Service Listings will be identified with an appropriate code or symbol (e.g. “LS”) in MLS compilations so potential cooperating brokers will be aware of the extent of  the  services  the  Listing  Broker  will  provide  to  the  seller(s),  and  any  potential  for cooperating brokers being asked to provide some or all of these services to Listing Broker’s clients, prior to initiating efforts to show or sell the property.

7.2.2 Scope of Service; MLS Entry-Only Listings
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
MLS Entry-Only Listings are listings whereby the Listing Broker, pursuant to the listing agreement, will not provide any of the following services:

a. provide  cooperating brokers  with  any  additional  information  regarding  the property not already displayed in the MLS but instead gives cooperating brokers authority to contact the seller(s) directly for further information;

b. accept  and  present  to  the  seller(s)  offers  to  purchase  procured  by  cooperating brokers  but  instead  gives  cooperating  brokers  authority  to  present  offers  to purchase directly to the seller(s);

c. advise the seller(s) as to the merits of officers to purchase;

d. assist the seller(s) in developing communicating, or presenting counter-offers; or

e. participates on the seller(s) behalf in negotiations leading to the sale of the listed property.

Said MLS Entry-Only listings will be identified with an appropriate code or symbol (e.g. “EO”) in MLS compilations so potential cooperating brokers will be aware of the extent of  the  services  the  Listing  Broker  will  provide  to  the  seller(s),  and  any  potential  for cooperating brokers being asked to provide some or all of these services to Listing Broker’s clients, prior to initiating efforts to show or sell the property.

7.2.3 Scope of Service; Legal Obligations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The  scopes  of  service  classifications set forth in these rules do not alter any obligations otherwise imposed on real estate licensees under  California  law,  including  Department  of  Real  Estate  regulations,  statutory  law  and common law.  The MLSs acceptance or publication of listings eligible for MLS submission in no way constitutes a validation that said obligations have been met.

7.3 Types of Properties
-----------------------
The MLS shall accept listings that satisfy the requirements of these rules on the following types of property *(see definitions in Appendix B)*:

* Residential Class – detached, duet, patio home/villa, condominium, townhouse

* Residential Income Class

* Mobile Home Class 

* Lots & Land Class

* Lease Rental Class

* Commercial Residential Income Multi-Unit 5+ Class

* Commercial Business Opportunity

* Commercial Industrial for Sale

* Commercial Industrial for Lease

* Commercial Lots and Land

*See Appendix B for definitions of the above property types.*

It shall be the responsibility of the Broker Participant and real estate Subscriber to properly classify the class of property listed, and if necessary, obtain a legal opinion to determine the correct classification.  By specifying the class of property listed, the Listing Broker certifies that the listing falls under the classification designated.  The MLS shall have no affirmative responsibility to verify the property class of any listing filed with the service.  However, the MLS shall have the right to have legal  counsel  make  a  determination  as  to  the  classification  of  the  property  class  and  if  the  Listing Broker  does  not  reclassify  it  accordingly,  the  AOR/Regional  MLS  shall  have  the  right  to  reject  or remove  any  such  listing  that  it  determines  falsely  represents  the  property  class  of  the  listing.  Submission of duplicate listings by the same Participant within the same property class is prohibited.

*For violation of this section, see Appendix A, Citable Infractions, 3.1.7, Submission of a Duplicate Listing within the Same Property Class.*

7.4 Compliance with California and Federal Law
----------------------------------------------
Notwithstanding  any  other  provision  of these MLS rules and regulations to the contrary, the service shall accept any listing that is required to accept under California or federal law.

7.4.1 Time Frame Definitions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Unless otherwise expressly indicated, where compliance time frames set forth days, “days” mean calendar days; “days after” means the specified number of calendar days after the occurrence of the event specified, not counting the calendar date on which the specified event occurs, and ending at 11:59 p.m. on the final day; and “days prior” means the specified number of calendar days before the occurrence of the event specified, not counting the calendar date on which the specified event is scheduled to occur.

7.5 Mandatory Submission
------------------------
Within 3 days after  all necessary signatures of  the seller(s) have been  obtained  on  the  listing  or  at  the  beginning  date  of  the  listing  as  specified  in  the  contract, whichever  is  later,  on  any  exclusive  right  to  sell  or  exclusive  agency  listing  on  one  to four-unit residential property and vacant lots located within the service area of the MLS, Broker Participants shall 

(1) Input the listing to the service, or 

(2)  Submit  a  seller-signed  exclusion  in  accordance  with  Section  7.6  (Exempted  Listings)  to  the service.

All necessary signatures are those needed to create an enforceable listing, which generally means all named signatories to the listing agreement. In the event there are known additional property owners not  made  a  signatory  to  the  listing,  listing broker  shall  disclose  said  fact  on  the  service  and  state whether  the  listed  seller  will  make  the  sale  contingent  on  the  consent  of  the  additional  property 
owners.  In  the  event  listing  agent  is  prevented  from  complying  with  the 
3-daytime  period  due  to seller’s delay in returning the signed listing agreement, listing broker must submit the listing to the service  within  3  days  of  receipt  back  from  seller.  The  MLS  may  require  listing  broker  to  present documentation to the service evidencing seller’s delayed transmission.  Only  those  listings  that  are within the service area of the MLS must be input. Open listings or listings of property located outside the MLS's service area (see Section 7.7) are not required by the service, but may be input at the Broker Participant’s option. 

*For violation of this section, see Appendix A, Citable Infractions, 2.1, Listing Not Loaded Within 3 days of Start Date of Listing.*

7.6 Exempted Listings
---------------------
If  the  seller  refuses  to  permit  the  listing  to  be  disseminated  by  the service, the listing broker shall submit to the service a certification signed by the seller that the seller does not authorize the listing to be disseminated by the service. C.A.R. Standard Form SELM may be used for this certification, but in any event, said exclusion shall include an advisory to seller that, in keeping the listing off the MLS

(1) Real estate agents and brokers from other real estate offices, and their buyer clients, who have access to the MLS may not be aware seller’s property is for sale,

(2) Seller’s property will not be included in the MLS’s download to various real estate Internet sites that are used by the public to search for property listings, and 

(3) Real estate agents, brokers and members of the public may be unaware of the terms and conditions under which seller is marketing the property

(4)  The  reduction  in  exposure  of  the  listing  may  lower  the  number  of  offers  made  and negatively impact the sales price

.. note:: NOTE  FOR  THE  FUTURE: Parallel  language  along  the  line  of  that  set  forth  below  will  be  added  as  an additional disclosure requirement under the Exempted Listing Rule once it and/or any other similar or related change is made to the SEL and goes into effect in the C.A.R. Standard Forms (the process of which is currently underway): and (4) the reduction in exposure of the listing may lower the number of offers made on the property and may adversely impact the overall price.

*For violation of this section, see Appendix A, Citable Infractions, 2.2, Listing Waiver Not Submitted to MLS within 3 days of Start Date of Listing.*

7.7 Service Area
----------------
The MLS shall service Alameda and Contra Costa Counties.  If EBRDI enters  into  regional  MLS  agreements  or  a  regional  MLS  corporation  with  other MLSs  and  has enlarged the service area as part of the agreement or corporation, submission of the type of listings specified  in  section  7.5  is  mandatory  for  the  area  covered  by  the  combined  service  areas  of  the Associations signatory to the regional MLS agreement or part of the regional MLS corporation.

7.8 Change of Listing Information
---------------------------------
Listing  Brokers  shall  input  any  change  in  listing information, including the listed price or other change in the original listing agreement, to the MLS within 1 day  after the authorized change is received by the Listing Broker.  By inputting such changes to  the  MLS,  the  Listing  Broker  represents  that  the  listing  contract  has  been  modified  in  writing  to reflect  such  change  or  that  the  Listing  Broker  has  obtained  other  legally  sufficient  written authorization to make such change.

*For violation of this section, see Appendix A, Citable Infractions, 2.3, Status Changes Not Reported by Deadline.*

7.9 Withdrawal of Listing Prior to Expiration
---------------------------------------------
The  Listing  Broker must withdraw listings  of  property  from  the  MLS  before  the  expiration  date  of  the  listing  agreement  provided  the Listing Broker has received written instructions from the seller to withdraw the listing from the MLS. Listing broker may withdraw any listing from the MLS 48 hours after providing seller with written notice of the broker’s intention to withdraw the listing based on a dispute with the seller regarding the terms of the listing agreement.  The MLS may require the listing broker to provide a copy of any notice of dispute or any written instructions from the seller. Sellers do not have the unilateral right to require the MLS to cancel any listing.  However, the MLS reserves the right to remove a listing from the MLS database if the seller can document that his or her listing agreement with the Listing Broker has been terminated or is invalid.  Withdrawal from the MLS with the seller’s consent does not relieve the obligation of the listing broker to report the sale and sales price if it closes escrow while the seller is represented by the listing broker.

*For violation of this section, see Appendix A, Citable Infractions, Section 3, Submission of Listings that Do Not Satisfy the Requirements of the MLS Rules.*

7.10 Contingencies
------------------
Any  contingency  or  condition  of  any term  in  a  listing  shall  be specified and noticed to the Participants and Subscribers.

7.11 Details on Listings Filed With the Service
-----------------------------------------------
Electronically input data or a property profile sheet, when filed with the service by the Listing Broker, all listings input into the MLS shall be complete in every detail as specified on the property profile sheet including full gross listing price, listing expiration date, compensation offered to other Broker Participants and any other item required to be included as determined by the EBRDI Board of Directors.  Listings that are incomplete shall be ineligible for publication in the MLS and subject to immediate removal.

*For violation of this section, see Appendix A, Citable Infractions, 3.1, Submission of Listings that DoNot Satisfy the Requirements of the MLS Rules.*

7.11.1 List Date Definition
~~~~~~~~~~~~~~~~~~~~~~~~~~~
For MLS Data tracking purposes the “List Date” field in the MLS shall be defined as the date of input into the MLS system.

7.12 Unilateral Contractual Offer; Sub-agency Optional
------------------------------------------------------
In  filing  a  property  with  the  MLS, the Broker Participant makes a blanket unilateral contractual offer of compensation to the other MLS Broker Participants for their services in selling the property.  Except as set forth in Rule 7.15 below or  pursuant  to  California  Civil  Code  Section  1087,  a  Broker  Participant  must  specify  some compensation to be paid to either a buyer’s agent or a sub-agent and the offer of compensation must be stated in one, or a combination of, the following forms (1) a percentage of the gross selling price; or  (2)  a  definite  dollar  amount.    The  amount  of  compensation  offered  through  the  MLS  may  not contain any provision that varies the amount of compensation offered based on conditions precedent or subsequent or on any performance, activity or event.  Furthermore, the MLS reserves the right to remove a listing from the MLS database that does not conform to the requirements of this section.  At the Broker Participant’s option, a Broker Participant may limit his or her  offer of compensation to buyer’s agents only, to sub-agents only, or make the offer of compensation to both.  Any limitations on the contractual offer of compensation must be specified on the property profile sheet.  The amount of compensation offered to buyers’ agents, sub-agents  may  be  the  same,  or  different  but  must  be clearly specified on the property profile sheet.  Broker Participants wishing to offer sub-agency to the other  MLS  Broker  Participants  must  so  specify  on  the property  profile  sheet  and  on  the  MLS, otherwise, the offer of compensation does not constitute an offer of sub agency.

7.13 Acceptance of Contractual Offer
------------------------------------
The Broker Participant’s contractual offer (with or  without  sub-agency)  is  accepted  by  the  Participant/selling  broker  by  procuring  a  buyer  which ultimately  results  in  the  creation  of  a  sales  or  lease  contract.    Payment  of  compensation  by  the Participant/Listing Broker to the Participant/cooperating broker under this section is contingent upon either (1) the final closing or (2) the Participant/Listing Broker’s receipt of monies resulting from the seller or buyer’s default of the underlying sales or lease contract.  Notwithstanding this section, the Listing Broker and/or cooperating broker shall retain any remedies they may have against either the buyer or seller due to a default under the terms of the purchase agreement, listing agreement or other specific contract.  Any dispute between Participants arising out of this section shall be arbitrated under section 16 of these rules and shall not be considered a rules violation.

7.14 Consent to Act as Dual Agent
---------------------------------
By  offering  compensation  and/or  sub-agency  to Broker Participants, the Listing Broker is _not automatically representing that the seller has consented to  the  cooperating  broker  acting  as  a  dual  agent  representing  both  the  buyer  and  the  seller.    No cooperating broker shall act as both an agent of the buyer and the seller without first contacting the Listing Broker and ascertaining that the seller has consented to such dual agency.

7.15 Estate Sale, Probate and Bankruptcy Listings
-------------------------------------------------
Compensation offered through the MLS to cooperating brokers on estate sale, probate or bankruptcy listings is for the amount published therein  as  long as  the  cooperating  broker  produces  the  contract  which  is  ultimately  successful  and confirmed by the court, if court confirmation is required.  In the event the contract produced by the cooperating broker is overbid in court and the overbid contract is confirmed, the original cooperating broker shall receive the amount of compensation specified as “unconfirmed cooperating broker’s compensation” or “u.c.b.” in the property data profile sheet and on the MLS.  For estate sale or probate listings,  the  compensation  offered  through  the  service  under  these  rules  and  this  section  shall  be considered an agreement as referred to in California Probate Code Section 10165 and will therefore supersede  any  commission  splits  provided  by  statute  when  there  is  not  agreement. This  section contemplates that estate sale; probate and bankruptcy judges have broad discretion and therefore are not intended as a guarantee of a specific result as to commissions in every probate or bankruptcy sale.

7.16 Changes to Offer of Compensation by Listing Broker to All Broker Participants
----------------------------------------------------------------------------------
The  Listing  Broker  may  from  time  to  time,  adjust  the  published  compensation  offered  to  all  MLS Broker Participants for their services with respect to any listing by changing the compensation offered on the MLS or providing written notice to the MLS of the change.  Any changes in compensation will be effective after the change is published in the MLS, either through electronic transmission or printed form, whichever occurs first.  The Listing Broker may revoke or modify the offer of compensation in advance as to an individual Broker Participant prior to acceptance in accordance with general contract principles but in no event shall the Listing Broker revoke or modify the offer later than the time the cooperating broker (a) physically delivers or transmits by fax or e-mail to the listing broker a signed offer from a prospective buyer to purchase the property for which the compensation has been offered through the MLS, or (b) notifies the Listing Broker in person or by telephone, fax or e-mail that the cooperating broker is in possession of a signed offer from a prospective buyer to purchase the property for which the compensation has been offered through the MLS and is awaiting instructions from the Listing  Broker  as  to  the  manner  of  presentation  or  delivery  of  that  offer.    Any  such  independent advance revocations, modifications of the offer or agreements between real estate brokers are solely the responsibility of such brokers and shall not be submitted to, shall not be published by, or governed in any way by the service.

7.17 Broker Participant or Real Estate Subscriber as Principal
--------------------------------------------------------------
If a Listing Broker has any  interest  in  property,  the  listing  of  which  is  to  be  disseminated  through  the  service,  that  person shall disclose that interest on the MLS.

7.18 Multiple Unit Properties
-----------------------------
All properties which are to be sold or which may be sold separately must be indicated individually on the MLS and will be published separately.  When part of a listed property has been sold, the Listing Broker shall input the appropriate changes on the MLS.

7.19 Expiration, Extension, and Renewal of Listings
---------------------------------------------------
Listings  shall  be  removed  from the  MLS  database  on  the  expiration  date  specified  on  the  listing  unless  the  listing  is  extended  or renewed  by  the  Listing  Broker.    This  Listing  Broker  shall  obtain  written  authorization  from  the seller(s) before filing any extension or renewal of a listing.  Any renewals or extensions received after the expiration date of the original listing shall be treated as a new listing and will be subject to any fees applicable to new listings.  At any time and for any reason, the MLS has the right to request a copy of the seller’s written authorization to extend or renew a listing.  If a Listing Broker is requested to provide a copy of such authorization and does not do so within 1 day after the request, the listing shall be subject to immediate removal from the MLS.

*For violation of this section, see Appendix A, Citable Infractions, 3.4, Purposely Manipulating the MLS System to Circumvent the Rules.*

7.20 Listings of Participants or Subscribers Suspended, Expelled or Resigned
----------------------------------------------------------------------------

7.20.1 Failure to Pay MLS Fees; Resignation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
When a Participant or Subscriber is suspended  or  expelled  from  the  service  for  failure  to pay  MLS  fees  or  charges,  or  if  the Participant or Subscriber resigns from the service, the MLS shall cease to provide services to such Participant or Subscriber, including for  Broker Participants the  continued inclusion of listings  in  the  MLS  compilation  of  current  listing  information.    In  the  event  listings  are removed  from  the  MLS  pursuant  to  this  section,  it  shall  be  the  sole  responsibility  of  the Participant to notify the seller(s) that the property is no longer listed in the MLS.

7.20.2 Violation of MLS Rules
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
When a Participant or Subscriber is suspended or expelled from  the  service for  a  violation  of  the  MLS  rules  and  regulations,  the  MLS  shall cease to provide services to such participant or subscriber except that the listings in the MLS at the time of suspension or expulsion shall, at the suspended or expelled Participant’s option, be  retained  in  the  MLS  compilation  of  current  listing  information  until  sold,  withdrawn  or expired, and shall not be renewed or extended by the MLS beyond the termination date of the listing  agreement  in  effect  when  the  expulsion  became  effective.    In  the  event  listings  are removed from the MLS pursuant to this section, it shall be the responsibility of the Participant to notify the seller(s) that the property is no longer in the MLS.  If  a suspended or  expelled Participant opts to keep listings in the MLS until sold, withdrawn or expired under this Section 7.20.2, the Participant must comply with all applicable MLS rules and regulations during such time or the MLS may immediately remove the listings from further display.

7.21 No Control of Commission Rates or Fees Charged by Participants
-------------------------------------------------------------------
The MLS shall not fix, control, recommend, suggest, or maintain commission rates or fees for services to be rendered by Participants.  Further, the MLS shall not fix, control, recommend, suggest, or maintain the division of  commissions  or  fees  between  cooperating  Participants  or  between  Participants  and  non-Participants.

7.22 Dual or Variable Rate Commission Arrangements
--------------------------------------------------
The  Listing  Broker  shall disclose  the  existence  of  a  dual  or  variable  commission  arrangement  by  a  key,  code  or  symbol  as required  by  the  MLS.    A  dual  or  variable  rate  commission  arrangement  is  one  in  which  the  seller agrees or owner agrees to pay a specified commission if the property is sold by the Listing Broker without assistance and a different commission if the sale results through the efforts of a cooperating broker, or one in which the seller agrees to pay a specified commission if the property is sold by the Listing  Broker  either  with  or  without  the  assistance  of  a  cooperating  broker  and  a  different commission if the sale results through the efforts of a seller or owner.  The Listing Broker shall, in response to inquiries from potential cooperating brokers, disclose the differential that would result in either a cooperative transaction or, alternatively, in a sale that results through the efforts of the seller or owner.   If the cooperating broker is representing a buyer or tenant, the cooperating broker must then  disclose  such  information  to  his  or  her  client  before  the  client  makes  an  offer  to  purchase  or lease.

7.23 Right of Listing Broker and Presentation of Counter Offers
---------------------------------------------------------------
The Listing Broker has the right to participate in the presentation of any counter-offer made by the seller or lessor.  The Listing Broker does not have the right to be present at any discussion or evaluation of a counter-offer by  the  purchaser  or  lessee  (except  where  the  cooperating  broker  is  a  sub-agent).    However,  if  the purchaser or lesseegives written instructions to the cooperating broker that the Listing Broker not be present when a counter-offer is presented, the Listing Broker has the right to a copy of the purchaser’s or lessee’s written instructions.

7.24 Auction Listings
---------------------
Only  auction  listings which  comply  with  these  MLS  Rules  and Regulations,  including,  but  not  limited  to  Section  7.12  and  7.13,  may  be  submitted  to  the  Service. Auction listings entered into the MLS system shall have listing contracts as required under these rules, be clearly labeled as auction listing, and provide all the terms and conditions of the auction. Reserve auctions are not permitted on the MLS. 

Auction listings shall further specify the following:

a) The list price, which shall be seller’s minimum acceptable bid price;

b) The date, time and place of the auction;

c) All required procedures for Participants/Subscribers to register their representation of a potential bidder;

d) The amount of the buyer’s premium, if any; 

e) The time or manner in which potential bidders may inspect the listed property; 

f) Whether  or  not  the  seller  will  accept  a  purchase  offer prior to  the  scheduled auction; and
 
g) Any other material rules or procedures for the auction. Subsections (b) through (g) above shall not appear in a listing’s public remarks.

*For violation of this section, see Appendix A, Citable Infractions, 3.4, Purposely Manipulating the MLS System to Circumvent the Rules.*

7.25 Co-Listings
----------------
Only the listings of Participants and Subscribers will be accepted by the MLS. Inclusion of co-listings where the co-listing broker/agent is not a Participant or Subscriber in the MLS is prohibited.

7.26 Churning of Listings/ReListings
------------------------------------
Manipulation of listing data, which misleads the public, participants and subscribers, is not permitted. Only when the listing Participant/Subscriber has signed a new listing agreement with the seller, shall the Participant/Subscriber be permitted to relist the property in the MLS as a “new” listing. Only when a property has been off the market for more than thirty (30) days will the days (CDOM and CDMLS) on the listing start at zero (0). This applies regardless of withdrawals, cancellations, extensions, expiration and /or other modifications to listing agreement.

7.27 REO & HUD Listings
-----------------------
REO and HUD listings entered into the MLS system shall have listing contracts as required under these rules and be clearly labeled as REO or HUD listings.  See Section 7.11 for more information.

7.28 Short Sale (Lender Approval) Listings
------------------------------------------
Participants must disclose potential short sales (defined as a transaction where title transfers, where the sale price is insufficient to pay the total of all liens and costs of sale and where the seller does not bring sufficient liquid assets to the closing to cure all deficiencies) when reasonably known  to the listing broker.  This section does not allow Participants  with  short  sale  listings  to  place  any  reduction  conditions  on  compensation  offered through the MLS for items such as lender reductions of the gross commission, short sale negotiator fees or other administrative costs of the transaction.  Any reductions from the commission for such items,  if  any,  should  be  factored  in  as  a  reduced  amount  the  listing  broker  initially  offers  to  a cooperating broker and may not be made a condition of the offer. 

7.29 Assume Identity
--------------------
Participants and Subscribers within the same firm/office may allow another subscriber access to their listings by allowing subscriber to assume their identity. The access is solely for clerical and administrative functions. The participant or subscriber shall be responsible for  the  conduct  of  the  Assume  Identity  User,  and  shall  be  linked  in  the  system.  Participant  and Subscriber shall immediately notify the MLS of any changes, additions or deletions of users. Assume Identity Users shall also be subject to the following requirements:

- Must have any fees or fines paid current;

- Participant  or  Subscriber  linked  to  the Assume  Identity  User  may  be  fined, disciplined or terminated for Assume Identity User’s misconduct.

