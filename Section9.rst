9. SELLING PROCEDURES
=====================

9.1 Showings and Negotiations
-----------------------------
Appointments for showings and negotiations with the seller for the purchase of listed property filed with the service shall be conducted through the Listing Broker except under the following circumstances:

a. the Listing Broker gives the cooperating broker specific authority to show and/or negotiate directly with the seller, or

b. after reasonable effort and no less than 1 day after, the cooperating broker cannot contact the Listing Broker or his representative.  However, the Listing Broker, at his option, may preclude  such  direct  negotiations  by  the  cooperating  broker  by  giving  notice  to  all Participants through the MLS.

In the event the Listing Broker is having all showings and negotiations conducted solely by the seller, the Listing Broker shall clearly set forth such fact in the listing information published by the service.

9.1.1 Showing Access
~~~~~~~~~~~~~~~~~~~~
Properties entered into the system must be available to show within 3 days subject to any tenants rights.

*For violation of this section, see Appendix A, Citable Infractions, 5.1, Showings and Access.*

9.2 Disclosing the Existence of Offers
--------------------------------------
Listing  brokers,  in  response  to  inquiries  from buyers or cooperating brokers, shall, with the sellers’ approval, disclose the existence of offers on the property.  Where disclosure is authorized, the Listing Broker shall also disclose whether the listing licensee, by another licensee in the listing firm, or by a cooperating broker obtained offers.

9.3 Availability to Show or Inspect
-----------------------------------
Listing  Brokers  shall  not  misrepresent  the availability of access to show or inspect a listed property.

9.4 Presentation of Offers
--------------------------
The Listing Broker must arrange to present the offer as soon as possible, or give the cooperating broker a satisfactory reason for not doing so.  In the event a Listing Broker will not be participating in the presentation of offers, the Listing Broker shall clearly indicate this fact and it shall be disseminated to all Participants by the service.

9.5 Submission of Offers and Counter Offers
-------------------------------------------
The Listing Broker shall submit to the seller/landlord  all  offers  until  closing  unless  precluded  by  law,  governmental  rules  or  expressly instructed  by  the  seller/landlord  otherwise.    The  cooperating  broker  acting  for  buyer/tenant  shall submit to buyer/tenant all offers and counter-offers until acceptance.

9.6 Right of Cooperating Broker in Presentation of Offer
--------------------------------------------------------
The cooperating broker has the right to participate in the presentation of any offer to purchase he secures.  The cooperating broker does not have the right to be present at any discussion or evaluation of that offer by the seller and the Listing Broker.  However, if the seller gives written instructions to the Listing Broker requesting that the cooperating broker not be present when an offer the cooperating broker secured is presented, the cooperating broker shall convey the offer to the Listing Broker for presentation.  In such event, the cooperating broker shall have the right to receive a copy of the seller’s written instructions.  Nothing in this section diminishes or restricts the Listing Broker’s right to control the establishment of appointments for offer presentations.

9.7 Change of Compensation Offer by Cooperating Broker
------------------------------------------------------
Cooperating  Broker Participants and real estate Subscribers shall not use the terms of an offer to purchase to attempt to modify the Listing Broker’s offer of compensation to buyer’s agents nor make the submission of an executed  offer  to  purchase  contingent on the Listing Broker’s agreement to modify the offer of compensation.  However, failure of a cooperating broker to comply with this rule shall not relieve a Listing Broker of the obligation to submit all offers to the seller as required by section 9.4.

9.8 Cooperating Broker as a Purchaser
-------------------------------------
If a cooperating broker wishes to acquire an interest in property listed with a Listing Broker, such contemplated interest shall be disclosed to the Listing Broker prior to the time an offer to purchase is submitted to the Listing Broker.

9.9 Physical Presence of Participant or Subscriber
--------------------------------------------------
A Participant or Subscriber must be physically present on the property at all times when providing access to a listed property unless the Seller has consented otherwise.

.. note:: Nothing in these rules shall preclude the Listing Broker and cooperating broker from entering into a mutual agreement to change cooperative compensation.
