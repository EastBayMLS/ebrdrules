\select@language {english}
\contentsline {chapter}{\numberline {1}1. AUTHORITY}{2}{chapter.1}
\contentsline {chapter}{\numberline {2}2. PURPOSE}{3}{chapter.2}
\contentsline {chapter}{\numberline {3}3. NOT USED}{4}{chapter.3}
\contentsline {chapter}{\numberline {4}4. PARTICIPATION AND AUTHORIZED ACCESS}{5}{chapter.4}
\contentsline {chapter}{\numberline {5}5. MLS FEES AND CHARGES}{10}{chapter.5}
\contentsline {chapter}{\numberline {6}6. REGIONAL AND RECIPROCAL AGREEMENTS}{12}{chapter.6}
\contentsline {chapter}{\numberline {7}7. LISTING PROCEDURE}{13}{chapter.7}
\contentsline {chapter}{\numberline {8}8. DOCUMENTATION; PERMISSION; ACCURACY OF INFORMATION}{22}{chapter.8}
\contentsline {chapter}{\numberline {9}9. SELLING PROCEDURES}{24}{chapter.9}
\contentsline {chapter}{\numberline {10}10. REPORTING STATUS CHANGES AND OTHER INFORMATION TO THE MLS}{26}{chapter.10}
\contentsline {chapter}{\numberline {11}11. OWNERSHIP OF MLS COMPILATIONS AND COPYRIGHTS}{28}{chapter.11}
\contentsline {chapter}{\numberline {12}12. PROHIBITIONS AND REQUIREMENTS}{31}{chapter.12}
\contentsline {chapter}{\numberline {13}13. LOCKBOXES}{48}{chapter.13}
\contentsline {chapter}{\numberline {14}14. VIOLATIONS OF RULES AND REGULATIONS}{51}{chapter.14}
\contentsline {chapter}{\numberline {15}15. PROCEDURES FOR MLS RULES HEARINGS}{53}{chapter.15}
\contentsline {chapter}{\numberline {16}16. ARBITRATION}{54}{chapter.16}
\contentsline {chapter}{\numberline {17}17. NONPAYMENT OF MLS FEES}{56}{chapter.17}
\contentsline {chapter}{\numberline {18}18. CHANGES IN RULES AND REGULATIONS}{57}{chapter.18}
\contentsline {chapter}{\numberline {19}APPENDIX A - CITABLE INFRACTIONS}{58}{chapter.19}
\contentsline {chapter}{\numberline {20}APPENDIX B - DEFINITION OF RESIDENTIAL PROPERTY TYPES/CLASS}{67}{chapter.20}
