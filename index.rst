.. MLS Rules documentation master file, created by
   sphinx-quickstart on Fri Nov 25 13:55:24 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to EBRD MLS Rules and Regulations!
==========================================

Contents:

.. toctree::
   :maxdepth: 1

   Section1
   Section2
   Section3
   Section4
   Section5
   Section6
   Section7
   Section8
   Section9
   Section10
   Section11
   Section12
   Section13
   Section14
   Section15
   Section16
   Section17
   Section18
   APPENDIX_A
   APPENDIX_B
   

